package com.example.arvilsproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArvilsProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArvilsProjectApplication.class, args);
	}

}
